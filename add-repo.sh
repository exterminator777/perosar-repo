#!/usr/bin/env bash

if ! hash apt 2>/dev/null;then
    echo -e "\e[91m[!] Termux Only\e[0m"
    exit 0
else
    echo -e "\n\e[1m[*] Welcome To PeroSar-Repo Installation...! \e[0m"
fi

os=$(uname -o)

case $os in
    Android)
	export REPOD="$PREFIX/etc/apt/sources.list.d/perosar-repo.list"
	export SAUCELIST="$PREFIX/etc/apt/sources.list.d"
	export PM="apt install -y"
	;;
    *)
	export REPOD="$PREFIX/etc/apt/sources.list.d/perosar-repo.list"
	export SAUCELIST="$PREFIX/etc/apt/sources.list.d"
	export PM="sudo apt install -y"
	export SUDO="sudo"
	;;
esac

echo -e "\n\e[1m[*] Installing Requirements...! \e[0m"
sleep 0.25

if ! hash gpg 2>/dev/null;then
    ${PM} gnupg 2&>1 &> /dev/null
else
    echo -e "\n\e[1m[*] GnuPG Installed...! \e[0m"
fi

# Adding gpg keys
echo -e "\n\e[1m[*] Checking Information...! \e[0m"
sleep 0.25
sleep 0.55
echo -e "\n\e[1m[*] Adding GPG Key...! \e[1m"
$SUDO curl --progress-bar -L --fail --retry 4 -O https://exterminator777.gitlab.io/perosar-repo/perosar-repo.key
$SUDO apt-key add knight-repo.key
$SUDO rm knight-repo.key

Arch() {
    arch=`dpkg --print-architecture`
    case $arch in
	aarch64)
	    echo -e "\n\e[1m[*] Adding PeroSar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sar's Repo :)\ndeb [arch=aarch64] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
	arm)
	    echo -e "\n\e[1m[*] Adding PeroSar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sar's Repo :)\ndeb [arch=arm] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
	armhf)
	    echo -e "\n\e[1m[*] Adding PeroSar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sar's Repo :)\ndeb [arch=arm] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
	i686)
	    echo -e "\n\e[1m[*] Adding PeroSar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sar's Repo :)\ndeb [arch=i686] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
	x86_64)
	    echo -e "\n\e[1m[*] Adding Pero_Sar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sars's Repo :)\ndeb [arch=aarch64] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
	*)
	    echo -e "\n\e[1m[*] Adding PeroSar's Repo...! \e[0m"
	    sleep 0.75
	    $SUDO printf "# Pero_Sar's Repo :)\ndeb [arch=aarch64,all] https://cyberknight777.gitlab.io/knight-repo/ knight main" > ${SAUCELIST}/knight-repo.list
	    ;;
    esac
}

# Repo Exists
if [ -f "$REPOD" ];then
    sleep 0.25
    echo -e "\n\e[1m[!] PeroSar's Repo Already Exists...! \e[0m"
    echo -ne "\n\e[1m[!] Do You Wish To Add This Repo Again...?(Y/N): \e[0m"
    read optn
    case $optn in
	Y | y)
	    Arch
	    ;;
	n | N)
	    ;;
	*)
	    exit 0
	    ;;
    esac
else
    Arch
fi
	sleep 0.95
	echo -e "\n\e[1m[*] Updating Termux Environment...! \e[0m"
	apt-get update
	echo -e "\n\e[1m[*] Install A Package By : apt install <pkg> \e[0m"
	echo -e "\n\e[1m[*] Homepage : https://gitlab.com/exterminator777/perosar-repo \e[0m"




